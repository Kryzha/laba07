#include <stdio.h>
//Використовуємо заголовочний файл з стандартними функціями мови програмування сі (для функції random)
#include <stdlib.h>
#include <time.h>
#include <main.h>
//Точка входу у программу
int main() {
  //Потрібно для роботи функції rand
  srand(time(NULL));

  cycles(); //Запуск програми з коренем квадратним
  arrays(); //Запуск програми з множенням матриці саму на себе

  return 0;
}

///Функція, для розрахування модуля, переданого числа
double module(double number) {
  if(number < 0) {
    number = number * (-1);
  }

  return number;
}

///Функція для розрахування квадрата поданого числа
double sqr(double number) {
  number = number * number;

  return number;
}

///Функція для розрахування кореня квадратного поданого числа
double sqrt_from_number(double given_number) {
  /** Задаемо змінні, якими будемо користуватись:
    початкове значення числа,
    значення числа після ітераційної формули Герона,
    модуль різниці цих чисел.
    Змінні задані у відповідності до переліку
  */
  double x0, x1, module_of_difference;

  ///Перед початком подальших розрахунків встановлюємо перші знечання для змінних
  x0 = given_number;
  //Для визначення x1 використовуємо ітераційну формулу Герона
  x1 = 0.5 * (x0 + given_number / x0);
  module_of_difference = module(x0 - x1);

  /** Для того, щоб розразувати корінь, нам потрібно повторяти розрахунок x1
    за допомогою формули Герона допоки модуль різниці x0 та x1 або квадрат цього модуля
    не був меншим за точність розрахування кореня квадратного помноженого на 2
  */
  while(module_of_difference >= ACCURACY * 2 || sqr(module_of_difference) >= ACCURACY * 2) {
    x0 = x1;
    x1 = 0.5 * (x0 + given_number / x0);
    module_of_difference = module(x0 - x1);
  }

  /// На виході з функції x0 буде дорівнювати нашому шуканому числу
  return x0;
}

int cycles() {
  double sqrt_number = rand() % 1000;///< Дві змінні, які містять в собі задане число, та результат відповідно
  double result_number = sqrt_from_number(sqrt_number);///< Вичисляємо корінь числа за допомогою написаної функції

  return 0;
}

int multiplex_matrica(int matrica_string[SIZE * SIZE]) {
  ///Створюємо матрицю, яка буде вміщати в себе результат множення початкової матриці
  int result_matrica[SIZE][SIZE] = {
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0}
  };

  /** За допомогою данних циклів перебираємо елементи матриць,
      цикли допомагають нам зробити множення матриці згідно правил
  */
  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {
        result_matrica[i][j] = 0;
        for (int k = 0; k < SIZE; k++) {
            result_matrica[i][j] += matrica_string[i * SIZE + k] * matrica_string[k * SIZE + j];
        }
    }
  }

  return 0;
}

int arrays() {
  ///Створюємо двовимірний массив (тобто матрицю) згідно заданих розмірів. Матриця квадратна. Заповнюємо матрицю данними
  int matrica[SIZE][SIZE] = {
    {2, 3, 4},
    {5, 4, 1},
    {0, 2, 3}
  }, matrica_string[SIZE * SIZE];

  ///Данний фрагмент коду перероблюэ двовимірний масив у одновимірний
  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {
      matrica_string[i * SIZE + j] = matrica[i][j];
    }
  }

  ///Створюємо матрицю, яка тримає в собі результат множення матриці на матрицю
  multiplex_matrica(matrica_string);
  ///Створюємо матрицю для запису фінального результату, оскільки в мові програмування сі не можна повертати масиви з функцій

  return 0;
}
